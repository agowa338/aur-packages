#!/bin/bash

cd /var/abs/local
git reset --hard
git clean -dffx
git submodule foreach git checkout master
git submodule update --init
git submodule foreach git checkout master
git submodule foreach git pull origin master
git submodule foreach git clean -dffx
git submodule foreach git reset --hard
