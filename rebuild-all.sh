#!/bin/bash
LocalAURRoot=/var/abs/local

cd $LocalAURRoot
# make sure all files are owned by the build user:
chown pacman:pacman . -R
# update the git repos using the build user
sudo -u pacman ./reset-aur-gits.sh

# Function to build the submodule
function build {
	cd $1
	sudo -u pacman makepkg PKGDEST="$LocalAURRoot/.build" PKGEXT=".pkg" CARCH="x86_64"
	cd ..
}
# Function to install the submodule
function install {
	cd .build
	# sudo pacman -U # ToDo
	pacman -U *.pkg --noconfirm
	cd $LocalAURRoot
}
# Function to install dependencies to submodule
function installdep {
	pacman -S $1 --noconfirm
}
# Remove uneeded package after build
function uninstall {
	pacman -R $1 --noconfirm
}
# Function import pgp key from server
function importpgp {
	gpg --keyserver keyserver.ubuntu.com --recv $1
}

# aic94xx-firmware
build aic94xx-firmware
install aic94xx-firmware

# android-sdk-build-tools
installdep lib32-gcc-libs
installdep lib32-zlib
build android-sdk-build-tools
install android-sdk-build-tools

# android-apktool
installdep java-runtime
build android-apktool
install android-apktool

# android-sdk
installdep jdk8-openjdk # java-environment
installdep lib32-alsa-lib
installdep lib32-openal
installdep lib32-libstdc++5
installdep lib32-libxv
installdep lib32-mesa
installdep lib32-ncurses
installdep lib32-sdl
installdep lib32-fontconfig
installdep lib32-libpulse
installdep swt
build android-sdk
install android-sdk

# android-google-play-apk-expansion
build android-google-play-apk-expansion
install android-google-play-apk-expansion

# android-sdk-platform-tools
build android-sdk-platform-tools
install android-sdk-platform-tools

# android-platform-25
build android-platform-25
install android-platform-25

# android-support
build android-support
install android-support

# android-studio-canary
build android-studio-canary
install android-studio-canary

# apkstudio-git
installdep unzip
build apkstudio-git
install apkstudio-git

# dislocker-git
installdep mbedtls
installdep ruby
installdep cmake
build dislocker-git
install dislocker-git

# dolphin-emu-git
installdep hidapi
installdep sfml
installdep enet
installdep miniupnpc
installdep portaudio
installdep soundtouch
installdep wxgtk
build dolphin-emu-git
install dolphin-emu-git

# fingerprint-gui
# Manual stepps required view documentation
# add user to plugdev group
# and modify PAM configuration
installdep libfprint
build fingerprint-gui
install fingerprint-gui

# freerdp-git
installdep nasm
build openh264-git
install openh264-git
build freerdp-git
install freerdp-git

# google-chrome
# Manual stepps required, remove hash sume check from PKGBUILD
installdep ttf-liberation
build google-chrome
install google-chrome

# kde-thumbnailer-odf
installdep extra-cmake-module
build kde-thumbnailer-odf
install kde-thumbnailer-odf

# spotify
importpgp 5CC908FDB71E12C2
build libcurl-openssl-1.0
install libcurl-openssl-1.0
importpgp D9C4D26D0E604491
build libopenssl-1.0-compat
install libopenssl-1.0-compat
# spotify
installdep rtmpdump
installdep libcurl-compat
build spotify
install spotify

# steam-fonts
build steam-fonts
install steam-fonts

# ttf-ms-fonts
build ttf-ms-fonts
install ttf-ms-fonts

# wine-git
installdep lib32-gettext
installdep lib32-glu
installdep lib32-libpcap
build wine-git
install wine-git

# winetricks
build winetricks
install winetricks

# ultravnc-viewer
installdep wine
build ultravnc-viewer
install ultravnc-viewer

# steam-wine-git
installdep winetricks
build steam-wine-git
install steam-wine-git

# teamviewer11
installdep lib32-libpng12
installdep lib32-libxinerama
installdep lib32-libjpeg6-turbo
build teamviewer11
install teamviewer11

# visual-studio-code
installdep gvfs
build visual-studio-code-bin
install visual-studio-code-bin

# sublime-text
build sublime-text
install sublime-text

# wd719x-firmware
installdep lha
build wd719x-firmware
install wd719x-firmware

# vivaldi
installdep w3m
build vivaldi
install vivaldi

# vivaldi-ffmpeg-codecs
installdep ninja
build vivaldi-ffmpeg-codecs
install vivaldi-ffmpeg-codecs

# remmina-git
installdep libunique
installdep vte3
installdep libgnome-keyring
installdep webkit2gtk
installdep intltool
installdep gst-libav
installdep libappindicator-gtk3
build remmina-git
install remmina-git
build remmina-plugin-teamviewer
install remmina-plugin-teamviewer
build remmina-plugin-ultravnc
install remmina-plugin-ultravnc
installdep firefox
build remmina-plugin-url
install remmina-plugin-url

# intellij-idea-ce
build intellij-idea-ce
install intellij-idea-ce

# networkmanager-l2tp
installdep xl2tpd
installdep libnma
build networkmanager-l2tp
install networkmanager-l2tp

# gst-plugin-libde265
installdep gst-plugins-bad
build libde265
install libde265
build gst-plugin-libde265
install gst-plugin-libde265

# powershell
installdep python-docutils
build proot
install proot
installdep liburcu
installdep asciidoc
installdep xmlto
build lttng-ust
install lttng-ust
installdep lldb
installdep clang
installdep llvm
build dotnet-cli-git
install dotnet-cli-git
build dotnet-host
install dotnet-host
build dotnet-runtime-2.0
install dotnet-runtime-2.0
build dotnet-sdk-2.0
install dotnet-sdk-2.0
build powershell-git
install powershell-git

# slack-desktop
installdep hunspell-en
build slack-desktop
install slack-desktop

# ansible-git
#installdep ????
build ansible-git
install ansible-git

# xpra-winswitch-svn
build python-lz4
install python-lz4
build python2-lz4
install python2-lz4
build rencode
install rencode
build python2-gtkglext
install python2-gtkglext
installdep python-crypto
build xpra-winswitch-svn
install xpra-winswitch-svn

# PlayOnLinux4
build gstreamer0.10
install gstreamer0.10
build gstreamer0.10-base
install gstreamer0.10-base
build wxgtk2.8
install wxgtk2.8
build wxpython2.8
install wxpython2.8
build playonlinux4-git
install playonlinux4-git

# Anydesk
build anydesk
install anydesk

#installdeb i3
build i3cat-git
install i3cat-git

# skype
build skype
install skype

# git-lfs
build git-lfs
install git-lfs

# discord
importpgp 0FC3042E345AD05D
importpgp 0FC3042E345AD05D
importpgp 0FC3042E345AD05D
build libc++
install libc++ # TODO: generates two backages, both need to be passed to pacman -U simultanously.
installdeb ttf-symbola
build discord
install discord

# docker-git
build perl-rpc-xml
install perl-rpc-xml
# comment out check of perl-rpc-xml, if it fails because of regex not matching for ipv6
importpgp 6689E64E3D3664BB
installdep audit
installdep perl-locale-gettext
installdep swig
build apparmor
#install apparmor
# Only apparmor-libapparmor needs to be installed
installdep go-md2man
build docker-git
install docker-git
uninstall swig
uninstall perl-locale-gettext
uninstall audit
uninstall perl-rpc-xml
uninstall go-md2man

# tor-browser-en
importpgp D1483FA6C3C07136
build tor-browser-en
install tor-browser-en

# cmpdl-bin (Curse Modpack Downloader)
build cmpdl-bin
install cmpdl-bin

# linux-git (Linux Kernel)
build linux-git
install linux-git # TODO: fix this line, linux-git generates multiple packages.

# multimc-git
installdeb xorg-xrandr
installdeb jre8-openjdk
installdeb jdk8-openjdk
build multimc-git
install multimc-git

# i2p
importpgp 85F345DD59683006
installdeb apache-ant
build java-service-wrapper
install java-service-wrapper
build i2p
install i2p

# winconn
installdeb yelp
installdeb python2-dbus
installdeb python2-distutils-extra
